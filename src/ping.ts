const Ping: nkruntime.RpcFunction = function (ctx: nkruntime.Context, logger: nkruntime.Logger, nk: nkruntime.Nakama): string | void {

    // Return a response indicating the server is alive
    return JSON.stringify({ message: "pong" });
};