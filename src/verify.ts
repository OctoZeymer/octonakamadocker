const VerifyRPC: nkruntime.RpcFunction = function (ctx: nkruntime.Context, logger: nkruntime.Logger, nk: nkruntime.Nakama, payload: string): string | void {
    
    const userId = ctx.userId;
    
    // Parse the payload string into JSON
    const payloadObject = JSON.parse(payload);

    // Extract the value associated with the "payload" key
    const payloadValue = payloadObject.payload;

    logger.debug(payloadValue);

    if (payloadValue === "eggKGKGldals2323562AAAAA") 
        {
        try 
        {
            const result = nk.sqlExec("UPDATE users SET verify_time = now() WHERE id = $1", [userId]);
            
            // Return success message
            logger.info("Email verification completed successfully.");
            return JSON.stringify({ message: "Email verification completed successfully." });
        } 
        catch (error) 
        {
            // Log error and return error message
            logger.error("Error updating user record:", error);
        }
    } 
    else 
    {
        logger.error("Error updating user record");
        return JSON.stringify({ message: "Can't verify" });
    }

    return JSON.stringify({});
};
