const deleteLeaderboard: nkruntime.RpcFunction = function (ctx: nkruntime.Context, logger: nkruntime.Logger, nk: nkruntime.Nakama, payload: string): string | void {
    
    let data;
    
    try {
        data = JSON.parse(payload);
    } catch (error) {
        throw new Error("Invalid JSON payload ");
    }

    const leaderboardId = data.leaderboardId;
    const ownerId = data.ownerId;

    if (typeof leaderboardId !== 'string' || leaderboardId.trim() === '') {
        throw new Error("Leaderboard ID must be a non-empty string.");
    }

    if (typeof ownerId !== 'string' || ownerId.trim() === '') {
        throw new Error("Owner ID must be a non-empty string.");
    }

    logger.info(`Deleting leaderboard with ID: ${leaderboardId} and owner ID: ${ownerId}`);

    const ADMIN_USER_ID = "f0cad66f-f754-42ba-a963-81665cbcdc05";
    if (ctx.userId !== ADMIN_USER_ID) {
        throw new Error(`You do not have permission to delete this record. Current userID: ${ctx.userId}`);
    }

    try {
        nk.leaderboardRecordDelete(leaderboardId, ownerId);
    } catch (error) {
        throw new Error("Failed to delete leaderboard record ");
    }

    return JSON.stringify({ success: true });
};