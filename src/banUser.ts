const banUser: nkruntime.RpcFunction = function (
    ctx: nkruntime.Context,
    logger: nkruntime.Logger,
    nk: nkruntime.Nakama,
    payload: string
): string | void {
    let data;

    try {
        data = JSON.parse(payload);
    } catch (error) {
        throw new Error("Invalid JSON payload.");
    }

    const userId = data.userId;

    if (typeof userId !== "string" || userId.trim() === "") {
        throw new Error("User ID must be a non-empty string.");
    }

    // Ban Boolean
    const storageKeyBan = {
        collection: "player_profile",
        key: "exclusion",
        userId: userId
    };

    let storageObjectBan;
    try {
        const storageObjects = nk.storageRead([storageKeyBan]);
        
        storageObjectBan = storageObjects.length > 0 ? 
        {
            collection: storageObjects[0].collection,
            key: storageObjects[0].key,
            userId: storageObjects[0].userId,
            value: storageObjects[0].value
        } : 
        {
            collection: "player_profile",
            key: "exclusion",
            userId: userId,
            value: { IsExcluded: true }
        };

        // Update the boolean value
        storageObjectBan.value.IsExcluded = true;

        // Write updated storage object
        nk.storageWrite([storageObjectBan]);
        logger.info(`Ban status for user ${userId} updated to true.`);
    } catch (error) {
        if (error instanceof Error) {
            throw new Error(`Failed to update ban status: ${error.message}. 
                Writing ban status object: ${JSON.stringify(storageObjectBan)}`);
        } else {
            throw new Error("Failed to update ban status: Unknown error occurred.");
        }
    }

    // Ban Reason
    const reason = data.reason;

    if (typeof reason !== "string" || reason.trim() === "") {
        throw new Error("Ban reason must be a non-empty string.");
    }

    const storageKeyReason = {
        collection: "player_profile",
        key: "banreason",
        userId: userId
    };

    try {
        const storageObjectsReason = nk.storageRead([storageKeyReason]);
        const storageObjectReason = storageObjectsReason.length > 0 ? 
        {
            collection: storageObjectsReason[0].collection,
            key: storageObjectsReason[0].key,
            userId: storageObjectsReason[0].userId,
            value: storageObjectsReason[0].value
        } : 
        {
            collection: "player_profile",
            key: "banreason",
            userId: userId,
            value: { BanReason: reason }
        };

        // Update the reason value
        storageObjectReason.value.BanReason = reason;

        // Write updated storage object
        nk.storageWrite([storageObjectReason]);
        logger.info(`Ban reason for user ${userId} updated to: ${reason}`);
    } catch (error) {
        if (error instanceof Error) {
            throw new Error(`Failed to update ban reason: ${error.message}`);
        } else {
            throw new Error("Failed to update ban reason: Unknown error occurred.");
        }
    }

    return JSON.stringify({ success: true });
};