const GetUTCTime: nkruntime.RpcFunction = function (ctx: nkruntime.Context, logger: nkruntime.Logger, nk: nkruntime.Nakama): string | void {
    
    const utcDate = new Date();

    const utcTimeStamp = utcDate.getTime();
   
    logger.info("Timestamps result: ", utcTimeStamp);

    return JSON.stringify( { time: utcTimeStamp });

};