var InitModule = function (ctx, logger, nk, initializer) {
    // Initialize RPC
    initializer.registerRpc('banUser', BanUserRPC);
    initializer.registerRpc('checkBanStatus', CheckBanStatusRPC);
    initializer.registerRpc('getUTCTime', GetUTCTime);
    initializer.registerRpc('submitScores', SubmitScoresRPC);
    var leaderboards = [
        {
            "authoritative": false,
            "id": "Cells",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }, {
            "authoritative": false,
            "id": "ModPoints",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }, {
            "authoritative": false,
            "id": "Shards",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }, {
            "authoritative": false,
            "id": "ResearchPoints",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }, {
            "authoritative": false,
            "id": "AcademyPoints",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }, {
            "authoritative": false,
            "id": "OuroCells",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }, {
            "authoritative": false,
            "id": "OuroModPoints",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }, {
            "authoritative": false,
            "id": "OuroShards",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }, {
            "authoritative": false,
            "id": "OuroResearchPoints",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }, {
            "authoritative": false,
            "id": "OuroAcademyPoints",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }, {
            "authoritative": false,
            "id": "OuroOrbs",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }, {
            "authoritative": false,
            "id": "OuroHunterXp",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING
        }
    ];
    for (var _i = 0, leaderboards_1 = leaderboards; _i < leaderboards_1.length; _i++) {
        var l = leaderboards_1[_i];
        try {
            nk.leaderboardCreate(l.id, l.authoritative, l.sort, l.operator, l.reset, l.metadata);
        }
        catch (e) {
            logger.error('unable to create leaderboard: %o', e);
        }
    }
    logger.info('Octocube game loaded.');
};
