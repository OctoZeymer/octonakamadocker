const InitModule: nkruntime.InitModule = function (ctx: nkruntime.Context, logger: nkruntime.Logger, nk: nkruntime.Nakama, initializer: nkruntime.Initializer) {

    initializer.registerRpc('banUser', banUser);
    initializer.registerRpc('deleteLeaderboard', deleteLeaderboard);
    initializer.registerRpc('checkBanStatus', CheckBanStatusRPC);
    initializer.registerRpc('getUTCTime', GetUTCTime);
    initializer.registerRpc('Verify', VerifyRPC);
    initializer.registerRpc('ping', Ping);
    initializer.registerBeforeAuthenticateCustom(beforeAuthenticateCustom);
    initializer.registerBeforeAuthenticateDevice(beforeAuthenticateDevice);
    initializer.registerBeforeAuthenticateApple(beforeAuthenticateApple);
    initializer.registerBeforeAuthenticateEmail(beforeAuthenticateEmail);
    initializer.registerBeforeAuthenticateFacebook(beforeAuthenticateFacebook);
    initializer.registerBeforeAuthenticateFacebookInstantGame(beforeAuthenticateFacebookInstantGame);
    initializer.registerBeforeAuthenticateGameCenter(beforeAuthenticateGameCenter);
    initializer.registerBeforeAuthenticateSteam(beforeAuthenticateSteam);

    const leaderboards = [
        {
            "authoritative": false,
            "id": "Cells",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING, 
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "ModPoints",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING, 
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "Shards",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING, 
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "ResearchPoints",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING, 
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "AcademyPoints",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING, 
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "OuroborosOrbs",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING, 
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "BorgeXp",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING, 
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "OzzyXp",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING, 
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "KnoxXp",
            "metadata": {},
            "operator": nkruntime.Operator.BEST,
            "reset": null,
            "sort": nkruntime.SortOrder.DESCENDING, 
            "enableRanks": true
        }
    ]

    for (const l of leaderboards) {
        try {
            nk.leaderboardCreate(l.id, l.authoritative, l.sort, l.operator, l.reset, l.metadata, l.enableRanks);
        } catch (e) {
            logger.error('unable to create leaderboard: %o', e)
        }
    }

    logger.info('Octocube game loaded.');
}

const beforeAuthenticateApple: nkruntime.BeforeHookFunction<nkruntime.AuthenticateAppleRequest> = (ctx, logger, nk, data) => {
    // Log a warning message
    logger.warn("Cant use apple");
    // Reject the authentication request by returning undefined
    return;
};
const beforeAuthenticateEmail: nkruntime.BeforeHookFunction<nkruntime.AuthenticateEmailRequest> = (ctx, logger, nk, data) => 
    {
        logger.debug("Email; ", data.account.email)
        if (data.account.email == "sn@octocubegames.com")
        {
            if  (!data.create)
                {
            return data;
                }
        }
        else
        {
        logger.warn("Email authentication is not allowed on this server!");
        // Reject the authentication request by returning undefined
        return;
        }
    };
    //const beforeUpdateAccount: nkruntime.BeforeHookFunction<nkruntime.UserUpdateAccount> = (ctx, logger, nk, data) =>
    //{
    //    let username = data.username;
    //    logger.debug("Username: ", username)
    //    if (username?.toLowerCase().indexOf("zeimar"))
    //    {
    //        logger.warn("Name not allowed");
    //    }
    //    else
    //    {
    //        return data;
    //    }
    //}
const beforeAuthenticateFacebook: nkruntime.BeforeHookFunction<nkruntime.AuthenticateFacebookRequest> = (ctx, logger, nk, data) => {
    // Log a warning message
    logger.warn("Cant use facebook");
    // Reject the authentication request by returning undefined
    return;
};
const beforeAuthenticateFacebookInstantGame: nkruntime.BeforeHookFunction<nkruntime.AuthenticateFacebookInstantGameRequest> = (ctx, logger, nk, data) => {
    // Log a warning message
    logger.warn("Cant use facebook instant");
    // Reject the authentication request by returning undefined
    return;
};
const beforeAuthenticateGameCenter: nkruntime.BeforeHookFunction<nkruntime.AuthenticateGameCenterRequest> = (ctx, logger, nk, data) => {
    // Log a warning message
    logger.warn("Cant use gamecenter");
    // Reject the authentication request by returning undefined
    return;
};
const beforeAuthenticateSteam: nkruntime.BeforeHookFunction<nkruntime.AuthenticateSteamRequest> = (ctx, logger, nk, data) => {
    // Log a warning message
    logger.warn("Cant use steam");
    // Reject the authentication request by returning undefined
    return;
};
const beforeAuthenticateCustom: nkruntime.BeforeHookFunction<nkruntime.AuthenticateCustomRequest> = (ctx, logger, nk, data) => {
    // Log a warning message
    logger.warn("Cant use custom");
    // Reject the authentication request by returning undefined
    return;
};
const beforeAuthenticateDevice: nkruntime.BeforeHookFunction<nkruntime.AuthenticateDeviceRequest> = (ctx, logger, nk, data) => {
    // Log a warning message
    logger.warn("Cant use device");
    // Reject the authentication request by returning undefined
    return;
};

