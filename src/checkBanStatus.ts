const CheckBanStatusRPC: nkruntime.RpcFunction = function (ctx: nkruntime.Context, logger: nkruntime.Logger, nk: nkruntime.Nakama, payload: string): string | void {
    // Ensure the payload is not empty
    if (!payload) {
        logger.error("Payload is empty");
        return;
    }

    // Use the payload as the userId directly
    const userId = payload;

    // Log the received userId
    logger.info("Received userId:", userId);
    // Query the database to check ban status
    //const query = `SELECT * FROM bans WHERE user_id = $1`;
    const query = `SELECT *`;
    logger.info("query: ", query);
    const queryArgs = [userId];
    const queryResult = nk.sqlQuery(query, queryArgs);
    logger.info("query result: ", queryArgs);

    // Check if the user is banned
    if (queryResult && queryResult.length > 0) {
        // User is banned
        return JSON.stringify({ banned: true });
    } else {
        // User is not banned
        return JSON.stringify({ banned: false });
    }
};