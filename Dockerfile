FROM node:alpine AS builder

WORKDIR /backend

COPY . .
RUN npx tsc

FROM heroiclabs/nakama:latest

COPY --from=builder /backend/build/*.js /nakama/data/modules/build/
COPY --from=builder /backend/local.yml /nakama/data/
