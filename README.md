Octocube Game
===

> The Nakama server code for the Octocube game.

### Prerequisites

The codebase requires these development tools:

* Node: v21.0.0 or greater.
* Docker Engine: 19.0.0 or greater.

### Development

The recommended workflow is to use Docker and the compose file to build and run the game server and database resources.

```shell
docker-compose up --build nakama
```

__NOTE__: Stop the containers with CTRL^C or the docker-compose `down` sub command.

#### Recompile / Run

When the containers have been started as shown above you can replace just the game server custom code and recompile it with the -d option.

```shell
docker-compose up -d --build nakama
```

#### Stop

To stop all running containers you can use the Docker compose sub-command.

```shell
docker-compose down
```

__NOTE__: You can wipe the database and workspace with `docker-compose down -v` to remove the disk volumes.

### Deployment

To deploy to the Heroic Cloud:

1. Build all code which will become part of the final container:

   ```shell
   npx tsc
   ```

2. Push the build artifacts into the repository and use the Heroic Cloud builder.

3. Select and deploy to the selected project within the Heroic Cloud.

