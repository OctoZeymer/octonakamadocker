"use strict";
var InitModule = function (ctx, logger, nk, initializer) {
    initializer.registerRpc('banUser', banUser);
    initializer.registerRpc('deleteLeaderboard', deleteLeaderboard);
    initializer.registerRpc('checkBanStatus', CheckBanStatusRPC);
    initializer.registerRpc('getUTCTime', GetUTCTime);
    initializer.registerRpc('Verify', VerifyRPC);
    initializer.registerRpc('ping', Ping);
    initializer.registerBeforeAuthenticateCustom(beforeAuthenticateCustom);
    initializer.registerBeforeAuthenticateDevice(beforeAuthenticateDevice);
    initializer.registerBeforeAuthenticateApple(beforeAuthenticateApple);
    initializer.registerBeforeAuthenticateEmail(beforeAuthenticateEmail);
    initializer.registerBeforeAuthenticateFacebook(beforeAuthenticateFacebook);
    initializer.registerBeforeAuthenticateFacebookInstantGame(beforeAuthenticateFacebookInstantGame);
    initializer.registerBeforeAuthenticateGameCenter(beforeAuthenticateGameCenter);
    initializer.registerBeforeAuthenticateSteam(beforeAuthenticateSteam);
    var leaderboards = [
        {
            "authoritative": false,
            "id": "Cells",
            "metadata": {},
            "operator": "best" /* nkruntime.Operator.BEST */,
            "reset": null,
            "sort": "descending" /* nkruntime.SortOrder.DESCENDING */,
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "ModPoints",
            "metadata": {},
            "operator": "best" /* nkruntime.Operator.BEST */,
            "reset": null,
            "sort": "descending" /* nkruntime.SortOrder.DESCENDING */,
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "Shards",
            "metadata": {},
            "operator": "best" /* nkruntime.Operator.BEST */,
            "reset": null,
            "sort": "descending" /* nkruntime.SortOrder.DESCENDING */,
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "ResearchPoints",
            "metadata": {},
            "operator": "best" /* nkruntime.Operator.BEST */,
            "reset": null,
            "sort": "descending" /* nkruntime.SortOrder.DESCENDING */,
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "AcademyPoints",
            "metadata": {},
            "operator": "best" /* nkruntime.Operator.BEST */,
            "reset": null,
            "sort": "descending" /* nkruntime.SortOrder.DESCENDING */,
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "OuroborosOrbs",
            "metadata": {},
            "operator": "best" /* nkruntime.Operator.BEST */,
            "reset": null,
            "sort": "descending" /* nkruntime.SortOrder.DESCENDING */,
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "BorgeXp",
            "metadata": {},
            "operator": "best" /* nkruntime.Operator.BEST */,
            "reset": null,
            "sort": "descending" /* nkruntime.SortOrder.DESCENDING */,
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "OzzyXp",
            "metadata": {},
            "operator": "best" /* nkruntime.Operator.BEST */,
            "reset": null,
            "sort": "descending" /* nkruntime.SortOrder.DESCENDING */,
            "enableRanks": true
        }, {
            "authoritative": false,
            "id": "KnoxXp",
            "metadata": {},
            "operator": "best" /* nkruntime.Operator.BEST */,
            "reset": null,
            "sort": "descending" /* nkruntime.SortOrder.DESCENDING */,
            "enableRanks": true
        }
    ];
    for (var _i = 0, leaderboards_1 = leaderboards; _i < leaderboards_1.length; _i++) {
        var l = leaderboards_1[_i];
        try {
            nk.leaderboardCreate(l.id, l.authoritative, l.sort, l.operator, l.reset, l.metadata, l.enableRanks);
        }
        catch (e) {
            logger.error('unable to create leaderboard: %o', e);
        }
    }
    logger.info('Octocube game loaded.');
};
var beforeAuthenticateApple = function (ctx, logger, nk, data) {
    // Log a warning message
    logger.warn("Cant use apple");
    // Reject the authentication request by returning undefined
    return;
};
var beforeAuthenticateEmail = function (ctx, logger, nk, data) {
    logger.debug("Email; ", data.account.email);
    if (data.account.email == "sn@octocubegames.com") {
        if (!data.create) {
            return data;
        }
    }
    else {
        logger.warn("Email authentication is not allowed on this server!");
        // Reject the authentication request by returning undefined
        return;
    }
};
//const beforeUpdateAccount: nkruntime.BeforeHookFunction<nkruntime.UserUpdateAccount> = (ctx, logger, nk, data) =>
//{
//    let username = data.username;
//    logger.debug("Username: ", username)
//    if (username?.toLowerCase().indexOf("zeimar"))
//    {
//        logger.warn("Name not allowed");
//    }
//    else
//    {
//        return data;
//    }
//}
var beforeAuthenticateFacebook = function (ctx, logger, nk, data) {
    // Log a warning message
    logger.warn("Cant use facebook");
    // Reject the authentication request by returning undefined
    return;
};
var beforeAuthenticateFacebookInstantGame = function (ctx, logger, nk, data) {
    // Log a warning message
    logger.warn("Cant use facebook instant");
    // Reject the authentication request by returning undefined
    return;
};
var beforeAuthenticateGameCenter = function (ctx, logger, nk, data) {
    // Log a warning message
    logger.warn("Cant use gamecenter");
    // Reject the authentication request by returning undefined
    return;
};
var beforeAuthenticateSteam = function (ctx, logger, nk, data) {
    // Log a warning message
    logger.warn("Cant use steam");
    // Reject the authentication request by returning undefined
    return;
};
var beforeAuthenticateCustom = function (ctx, logger, nk, data) {
    // Log a warning message
    logger.warn("Cant use custom");
    // Reject the authentication request by returning undefined
    return;
};
var beforeAuthenticateDevice = function (ctx, logger, nk, data) {
    // Log a warning message
    logger.warn("Cant use device");
    // Reject the authentication request by returning undefined
    return;
};
var banUser = function (ctx, logger, nk, payload) {
    var data;
    try {
        data = JSON.parse(payload);
    }
    catch (error) {
        throw new Error("Invalid JSON payload.");
    }
    var userId = data.userId;
    if (typeof userId !== "string" || userId.trim() === "") {
        throw new Error("User ID must be a non-empty string.");
    }
    // Ban Boolean
    var storageKeyBan = {
        collection: "player_profile",
        key: "exclusion",
        userId: userId
    };
    var storageObjectBan;
    try {
        var storageObjects = nk.storageRead([storageKeyBan]);
        storageObjectBan = storageObjects.length > 0 ?
            {
                collection: storageObjects[0].collection,
                key: storageObjects[0].key,
                userId: storageObjects[0].userId,
                value: storageObjects[0].value
            } :
            {
                collection: "player_profile",
                key: "exclusion",
                userId: userId,
                value: { IsExcluded: true }
            };
        // Update the boolean value
        storageObjectBan.value.IsExcluded = true;
        // Write updated storage object
        nk.storageWrite([storageObjectBan]);
        logger.info("Ban status for user ".concat(userId, " updated to true."));
    }
    catch (error) {
        if (error instanceof Error) {
            throw new Error("Failed to update ban status: ".concat(error.message, ". \n                Writing ban status object: ").concat(JSON.stringify(storageObjectBan)));
        }
        else {
            throw new Error("Failed to update ban status: Unknown error occurred.");
        }
    }
    // Ban Reason
    var reason = data.reason;
    if (typeof reason !== "string" || reason.trim() === "") {
        throw new Error("Ban reason must be a non-empty string.");
    }
    var storageKeyReason = {
        collection: "player_profile",
        key: "banreason",
        userId: userId
    };
    try {
        var storageObjectsReason = nk.storageRead([storageKeyReason]);
        var storageObjectReason = storageObjectsReason.length > 0 ?
            {
                collection: storageObjectsReason[0].collection,
                key: storageObjectsReason[0].key,
                userId: storageObjectsReason[0].userId,
                value: storageObjectsReason[0].value
            } :
            {
                collection: "player_profile",
                key: "banreason",
                userId: userId,
                value: { BanReason: reason }
            };
        // Update the reason value
        storageObjectReason.value.BanReason = reason;
        // Write updated storage object
        nk.storageWrite([storageObjectReason]);
        logger.info("Ban reason for user ".concat(userId, " updated to: ").concat(reason));
    }
    catch (error) {
        if (error instanceof Error) {
            throw new Error("Failed to update ban reason: ".concat(error.message));
        }
        else {
            throw new Error("Failed to update ban reason: Unknown error occurred.");
        }
    }
    return JSON.stringify({ success: true });
};
var CheckBanStatusRPC = function (ctx, logger, nk, payload) {
    // Ensure the payload is not empty
    if (!payload) {
        logger.error("Payload is empty");
        return;
    }
    // Use the payload as the userId directly
    var userId = payload;
    // Log the received userId
    logger.info("Received userId:", userId);
    // Query the database to check ban status
    //const query = `SELECT * FROM bans WHERE user_id = $1`;
    var query = "SELECT *";
    logger.info("query: ", query);
    var queryArgs = [userId];
    var queryResult = nk.sqlQuery(query, queryArgs);
    logger.info("query result: ", queryArgs);
    // Check if the user is banned
    if (queryResult && queryResult.length > 0) {
        // User is banned
        return JSON.stringify({ banned: true });
    }
    else {
        // User is not banned
        return JSON.stringify({ banned: false });
    }
};
var deleteLeaderboard = function (ctx, logger, nk, payload) {
    var data;
    try {
        data = JSON.parse(payload);
    }
    catch (error) {
        throw new Error("Invalid JSON payload ");
    }
    var leaderboardId = data.leaderboardId;
    var ownerId = data.ownerId;
    if (typeof leaderboardId !== 'string' || leaderboardId.trim() === '') {
        throw new Error("Leaderboard ID must be a non-empty string.");
    }
    if (typeof ownerId !== 'string' || ownerId.trim() === '') {
        throw new Error("Owner ID must be a non-empty string.");
    }
    logger.info("Deleting leaderboard with ID: ".concat(leaderboardId, " and owner ID: ").concat(ownerId));
    var ADMIN_USER_ID = "f0cad66f-f754-42ba-a963-81665cbcdc05";
    if (ctx.userId !== ADMIN_USER_ID) {
        throw new Error("You do not have permission to delete this record. Current userID: ".concat(ctx.userId));
    }
    try {
        nk.leaderboardRecordDelete(leaderboardId, ownerId);
    }
    catch (error) {
        throw new Error("Failed to delete leaderboard record ");
    }
    return JSON.stringify({ success: true });
};
var GetUTCTime = function (ctx, logger, nk) {
    var utcDate = new Date();
    var utcTimeStamp = utcDate.getTime();
    logger.info("Timestamps result: ", utcTimeStamp);
    return JSON.stringify({ time: utcTimeStamp });
};
var VerifyRPC = function (ctx, logger, nk, payload) {
    var userId = ctx.userId;
    // Parse the payload string into JSON
    var payloadObject = JSON.parse(payload);
    // Extract the value associated with the "payload" key
    var payloadValue = payloadObject.payload;
    logger.debug(payloadValue);
    if (payloadValue === "eggKGKGldals2323562AAAAA") {
        try {
            var result = nk.sqlExec("UPDATE users SET verify_time = now() WHERE id = $1", [userId]);
            // Return success message
            logger.info("Email verification completed successfully.");
            return JSON.stringify({ message: "Email verification completed successfully." });
        }
        catch (error) {
            // Log error and return error message
            logger.error("Error updating user record:", error);
        }
    }
    else {
        logger.error("Error updating user record");
        return JSON.stringify({ message: "Can't verify" });
    }
    return JSON.stringify({});
};
var Ping = function (ctx, logger, nk) {
    // Return a response indicating the server is alive
    return JSON.stringify({ message: "pong" });
};
